import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person'

class App extends Component {
  state = {
    persons : [
      { name: 'Hanine', age: 33},
      { name: 'Sofia', age: 30},
      { name: 'Assia', age: 26},
    ],
    otherState: 'some other value'
  }

  switchNameHandler = (newName) => {
    console.log('was clicked');
    // D'ont do this : this.state.persons[0].name = 'Abdelhanin'
    this.setState({
      persons : [
        { name: newName, age: 33},
        { name: 'Sofia', age: 30},
        { name: 'Assia', age: 26},
      ]
    })
  }

  render() {
    return (
      <div className="App">
        <h1>Hi, I am react App!</h1>
        <p>This my first paragraphe</p>

        <button onClick={() => this.switchNameHandler('Abdelhanin')}>Switch Name</button>
        <Person 
          name={this.state.persons[0].name} 
          age={this.state.persons[0].age} />
        <Person 
          name={this.state.persons[1].name} 
          age={this.state.persons[1].age} />
        <Person 
          name={this.state.persons[2].name} 
          age={this.state.persons[2].name} 
          click={this.switchNameHandler.bind(this, 'Benhammou')}> I am Hanine sister </Person>
        <Person />
      </div>
    );
    //return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'example of react'));
  }
}

export default App;